/**
 * sindri/logger.js
 *
 * Created by André Timermann on 10/09/2015
 *
 */
'use strict';

let moment = require('moment');

// Config e Winston não são compatível com NEXE, vamos mante-los como módulo
let winston = require('winston' + '');
let config = require('config' + '');

let logger;

// TODO: Parametrizar opções principais no config
// TODO: Ajustar Log de acordo com argumentos passado na Linha de Comando
logger = new winston.Logger({
    transports: [
        // new winston.transports.DailyRotateFile({
        //     level: config.get('log.filenameLevel'),
        //     filename: config.get('log.filename'),
        //     handleExceptions: false,
        //     json: false,
        //     maxsize: 5242880, //5MB
        //     maxFiles: 100,
        //     colorize: false,
        //     timestamp: getTimestamp
        // }),
        new winston.transports.Console({
            level: config.get('log.consoleLevel'),
            handleExceptions: false,
            json: false,
            colorize: true,
            //timestamp: getTimestamp
        })
    ],
    exitOnError: false
});

/**
 * Verifica se Está configurado para o Nível desejado
 * Ex:
 * 	if (logger.is('info')){ 'IMPRIME'  }
 *
 * @param  {[type]} level [description]
 * @return {[type]}       [description]
 */
logger.is = function(level) {

    // Vamos usar o Transporte Console como identificador de Level
    var nameLevel =  logger.transports.console.level;

    return logger.levels[nameLevel] <= logger.levels[level];

};

/**
 * Retorna se o Log foi configurado para ser carregado publicamente
 *
 * @return {boolean}
 */
logger.isPublic = function(){
    return config.get('log.public');
};

module.exports = logger;

/**
 * Retorna um timestap formatado
 *
 * @return {string}
 */
function getTimestamp() {
    return moment().format('HH:mm:ss.SS');
}
